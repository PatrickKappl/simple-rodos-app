#include "rodos.h"


class HelloWorld : public StaticThread<>
{
  void run() override
  {
    PRINTF("Hello World!\n");
  }
};

auto const helloWorld = HelloWorld();
